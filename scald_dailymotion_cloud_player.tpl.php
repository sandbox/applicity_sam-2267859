<?php
/**
 * @file
 *   Default theme implementation for the Scald Dailymotion player.
 */
?>
<!--
<iframe width="<?php print $vars['video_width'] ?>" height="<?php print $vars['video_height'] ?>" frameborder="0" src="http://www.dailymotion.com/embed/video/<?php print $vars['video_id'] ?>"></iframe>
-->
<iframe width="<?php print $vars['video_width'] ?>" height="<?php print $vars['video_height'] ?>" src="<?php print $vars['embed_url'] ?>" frameborder="0" allowfullscreen></iframe>